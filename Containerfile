FROM quay.io/centos/centos:stream10-development

RUN dnf update -y --refresh --setopt=tsflags=nodocs && \
    dnf install -y --setopt=tsflags=nodocs dnf-plugins-core && \
    dnf config-manager --set-enabled extras-common && \
    dnf swap -y centos-stream-release centos-release-autosd && \
    # dnf install -y --setopt=tsflags=nodocs centos-release-automotive && \
    # the following two lines are needed instead of the line above until all the packages are in Automotive SIG repo \
    curl https://gitlab.com/CentOS/automotive/rpms/centos-release-automotive/-/raw/main/CentOS-Stream-Automotive.repo?ref_type=heads >/etc/yum.repos.d/automotive.repo && \
    dnf copr enable -y @centos-automotive-sig/dui "centos-stream-10-$(rpm --eval %_arch)" && \
    dnf install -y --setopt=tsflags=nodocs \
       cvd2img \
       openssl \
       qemu-kvm \
       start-avm \
       vhost-device-scmi \
       vhost-device-sound \
       vhost-device-vsock \
       virglrenderer \
       # util-linux provides getopt used by run_start_avm.sh \
       util-linux \
       && \
    # injecting  manually until AutoSD 10 compose will be available \
    dnf install -y --setopt=tsflags=nodocs \
    https://kojihub.stream.centos.org/kojifiles/vol/koji02/packages/$(rpm -q qemu-kvm --queryformat "%{NAME}/%{VERSION}/%{RELEASE}/%{ARCH}/qemu-kvm-ui-dbus-%{VERSION}-%{RELEASE}.%{ARCH}.rpm") && \
    dnf clean all -y

# start-avm package already provides:
# /usr/bin/start_avm
# /usr/bin/start_cvd_tools
# /usr/share/start_avm/templates/.cuttlefish_config.json
# keeping here only to ease development.
COPY run_start_avm.sh start_cvd_tools start_avm.sh /opt/start-avm/
COPY templates /opt/start-avm/templates/

CMD ["/bin/bash", "/opt/start-avm/run_start_avm.sh"]
