.PHONY: clean

all: start_cvd_tools

start_cvd_tools: start_cvd_tools.c
	gcc -O2 -g -o $@ $<

clean:
	rm -rf start_cvd_tools

install:
	install -d $(DESTDIR)$(PREFIX)/usr/bin
	install -m 755 start_cvd_tools $(DESTDIR)$(PREFIX)/usr/bin/start_cvd_tools
	install -m 755 start_avm.sh $(DESTDIR)$(PREFIX)/usr/bin/start_avm
