# Contributing to start-avm

Welcome - and a big thank you for wanting to contribute!

Let's get you started. This project is licensed under [GPL-3.0-only](COPYING) License.
You should only submit code that you personally own or have permission to submit under this license - and only if you are ok with that.

## Steps to your contribution

Whether you have a big contribution incoming, a small typo fix, or any other type of contribution you would like to add, the process is the same:

1. Make sure you understand the [license](COPYING), and agree to the [Developer Certificate of Origin](https://github.com/arcalot/.github/blob/main/DCO.md).
2. Optionally check your code with [pre-commit tool](https://pre-commit.com/).
3. Submit a pull request to the [relevant repository](https://gitlab.com/CentOS/automotive/src/start-avm).
4. Wait for a project maintainer to review your contribution (See [CODEOWNERS](CODEOWNERS)).

## Requirement

:warning:
By contributing to start-avm, you certify that you understand and agree to the contents
of the [Developer Certificate of Origin](https://developercertificate.org/).
